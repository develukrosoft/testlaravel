<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'book',
], function () {
    Route::get('/get/{id}', 'BookController@getById')->name('api.book.get.byId');
    Route::post('/save', 'BookController@save')->name('api.book.save');
    Route::post('/search', 'BookController@search')->name('api.book.search');
    Route::get('/categories', 'BookController@categories')->name('api.book.categories');
});
