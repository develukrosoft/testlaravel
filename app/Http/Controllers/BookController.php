<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookSaveRequest;
use App\Http\Requests\BookGetByIdRequest;
use App\Http\Resources\BookCategoryResource;
use App\Http\Resources\BookPostResource;
use App\Models\BookCategory;
use App\Models\BookPost;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function save(BookSaveRequest $request)
    {
        if (BookCategory::whereIn('id', $request->get('book_category_ids'))->count()) {
            $book = BookPost::create($request->only(['ISBN', 'title', 'price', 'book_author_id']));
            $book->category_ids = $request->input('book_category_ids');
            return response()->json([new BookPostResource($book)], 201);
        } else {
            return response()->json([
                'message' => 'Categories not found',
                'errors' => ['book_category_ids' => 'The selected category ids is invalid.']
            ], 422);
        }
    }

    public function getById($id)
    {
        if ($book = BookPost::find($id)) {
            return response()->json([new BookPostResource($book)], 201);
        } else {
            return response()->json([], 404);
        }
    }

    public function search(Request $request)
    {
        $books = BookPost::query();
        if ($q = $request->get('author_name')) {
            $books = $books->rightJoin('book_authors', 'book_authors.id', '=',
                'book_posts.book_author_id');
            $books->where(function ($query) use ($q) {
                $query->where('book_authors.full_name', 'LIKE', '%' . $q . '%');
            });
        }
        if ($category = BookCategory::where('title', $request->get('category_name'))->first()) {
            $category_id = $category->id;
        } else {
            $category_id = $request->get('category_id', null);

        }
        if ($category_id) {
            $books =
                $books->rightJoin('book_post_categories', 'book_post_categories.book_post_id', '=',
                    'book_posts.id');
            $books->whereIn('book_post_categories.book_post_category_id',
                [$category_id]);
        }
        return response()->json(BookPostResource::collection($books->get()), 200);
    }

    public function categories()
    {
        return response()->json(BookCategory::all()->pluck('title'), 200);
    }

    //    public function getPost(Request $request, $slug)
    //    {
    //        $post = CoursePost::where(['slug' => $slug, 'is_active' => 1])->first();
    //        if ($post) {
    //            return response()->json(['post' => new CoursePostResource($post)], 200);
    //        } else {
    //            return response()->json([], 404);
    //        }
    //    }
}
