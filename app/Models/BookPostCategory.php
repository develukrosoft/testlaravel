<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookPostCategory extends Model
{
    protected $table = 'book_post_categories';
    protected $fillable = ['book_post_id', 'book_category_id'];
    public $timestamps = false;
}
