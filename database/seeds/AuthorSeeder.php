<?php

use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            'Robin Nixon',
            'Christopher Negus',
            'Douglas Crockford'
        ];
        foreach ($list as $full_name) {
            \App\Models\BookAuthor::firstOrCreate(['full_name' => $full_name]);
        }
    }
}
