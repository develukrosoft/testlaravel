<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookPost extends Model
{
    protected $fillable = ['ISBN', 'title', 'price', 'book_author_id'];
    protected $appends = ['category_ids'];

    public function categories()
    {
        return $this->belongsToMany(BookCategory::class, 'book_post_categories', 'book_post_id',
            'book_post_category_id');
    }

    public function getCategoryIdsAttribute()
    {
        return $this->categories->pluck('id')->toArray();
    }

    public function setCategoryIdsAttribute($ids)
    {
        if ($this->exists) {
            $this->categories()->sync($ids);
        }
    }

    public function author()
    {
        return $this->hasOne(BookAuthor::class, 'id', 'book_author_id')->withDefault();
    }
}
