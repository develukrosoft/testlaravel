<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookSaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ISBN' => 'required|regex:/^[+]*([0-9]{3})-[0-9]{10}$/',
            'title' => 'required|max:255',
            'price' => 'required|numeric',
            'book_author_id' => 'required|exists:book_authors,id',
            'book_category_ids' => 'required|array',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ISBN.regex' => 'Invalid ISBN',
        ];
    }
}
