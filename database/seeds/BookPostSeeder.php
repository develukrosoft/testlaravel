<?php

use Illuminate\Database\Seeder;

class BookPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'ISBN' => '978-1491918661',
                'title' => 'Learning PHP, MySQL & JavaScript: With jQuery, CSS & HTML5',
                'author' => 'Robin Nixon',
                'categories' => ['PHP', 'Javascript'],
                'price' => '9.99'
            ],
            [
                'ISBN' => '978-0596804848',
                'title' => 'Ubuntu: Up and Running: A Power User\'s Desktop Guide',
                'author' => 'Robin Nixon',
                'categories' => ['Linux'],
                'price' => '12.99'
            ],
            [
                'ISBN' => '978-1118999875',
                'title' => 'Linux Bible',
                'author' => 'Christopher Negus',
                'categories' => ['Linux'],
                'price' => '19.99'
            ],
            [
                'ISBN' => '978-0596517748',
                'title' => 'JavaScript: The Good Parts',
                'author' => 'Douglas Crockford',
                'categories' => ['Javascript'],
                'price' => '8.99'
            ],
        ];

        foreach ($list as $book) {
            $categories =
                \App\Models\BookCategory::whereIn('title', $book['categories'])->get()->pluck('id')
                    ->toArray();
            $author = \App\Models\BookAuthor::where(['full_name' => $book['author']])->first();
            if ($author && count($categories)) {
                $bookPost = \App\Models\BookPost::create([
                    'ISBN' => $book['ISBN'],
                    'title' => $book['title'],
                    'book_author_id' => $author->id,
                    'price' => $book['price'],
                ]);
                $bookPost->category_ids = $categories;
            }
        }
    }
}
