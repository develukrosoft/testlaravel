<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookPostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ISBN' => $this->ISBN,
            'title' => $this->title,
            'author' => $this->author->full_name,
            'categories' => $this->categories->pluck('title'),
            'price' => $this->price,
        ];
    }
}
