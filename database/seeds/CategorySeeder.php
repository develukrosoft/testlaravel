<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            'php' => 'PHP',
            'javascript' => 'Javascript',
            'linux' => 'Linux'
        ];
        foreach ($list as $slug => $title) {
            \App\Models\BookCategory::firstOrCreate(['slug' => $slug, 'title' => $title]);
        }
    }
}
