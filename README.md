#POST

POST:/api/book/save
Params:
ISBN (required)
title (required)
price (required)
book_author_id (required)
book_category_ids (required)

POST:/api/book/search
Params:
author_name (not required)
category_id (not required)
category_name (not required)


#GET

GET:/api/book/get/{id}

GET:/api/book/categories


