<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_posts', function (Blueprint $table) {
            $table->id();
            $table->string('ISBN');
            $table->string('title');
            $table->double('price');
            $table->unsignedBigInteger('book_author_id')->nullable();
            $table->timestamps();

            $table->foreign('book_author_id')
                ->references('id')
                ->on('book_authors')
                ->onDelete('set null');
        });

        Schema::create('book_post_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('book_post_id');
            $table->unsignedBigInteger('book_post_category_id');

            $table->foreign('book_post_id')
                ->references('id')
                ->on('book_posts')
                ->onDelete('cascade');
            $table->foreign('book_post_category_id')
                ->references('id')
                ->on('book_categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_post_categories');
        Schema::dropIfExists('book_posts');
    }
}
